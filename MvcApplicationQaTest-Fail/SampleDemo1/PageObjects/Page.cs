﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SampleDemo1.PageObjects
{
    public abstract class Page
    {
        protected IWebDriver browser;

        protected Page(IWebDriver browser)
        {
            this.browser = browser;
        }

        protected void WaitFor(By locator, TimeSpan timeToWait)
        {
            IWait<IWebDriver> wait = new WebDriverWait(this.browser, timeToWait);

            wait.Until(x => x.FindElement(locator));
        }

        protected void Type(By by, string value)
        {
            browser.FindElement(by);
        }

        protected void Do(Action<IWebElement> thisAction, By on, bool throwIfNotFound)
        {
            var element = browser.FindElement(on);

            if (element == null && throwIfNotFound)
                throw new NoSuchElementException(string.Format("Unable to find {0}", on));

            thisAction.Invoke(element);
        }

        public static bool waitTillElementisDisplayed(IWebDriver driver, By by, int timeoutInSeconds)
        {
            bool elementDisplayed = false;

            for (int i = 0; i < timeoutInSeconds; i++)
            {
                try
                {

                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                    wait.Until(drv => drv.FindElement(by));
                    elementDisplayed = driver.FindElement(by).Displayed;

                    if (elementDisplayed)
                    {
                        return elementDisplayed;
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(2000);
                    }

                    //if (timeoutInSeconds > 0)
                    //{
                    //    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                    //    wait.Until(drv => drv.FindElement(by));
                    //}
                    //elementDisplayed = driver.FindElement(by).Displayed;
                }
                catch
                { }
            }
            return elementDisplayed;

        }
    }
}
