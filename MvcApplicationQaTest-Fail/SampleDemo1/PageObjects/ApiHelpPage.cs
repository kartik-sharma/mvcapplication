﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Windows.Forms;
using OpenQA.Selenium;

namespace SampleDemo1.PageObjects
{
	public class ApiHelpPage : Page
	{
		private static class Locator
		{
			public static readonly By Home_Menu = By.LinkText("Home");
			public static readonly By API_Menu = By.LinkText("API");

		}

		public ApiHelpPage(IWebDriver browser)
            : base(browser)
        {

		}

	}
}
