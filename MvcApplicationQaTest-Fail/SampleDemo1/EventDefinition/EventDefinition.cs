﻿using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using System;
using System.IO;
using System.Drawing.Imaging;
using System.Text;
namespace SampleDemo1.EventDefinition
{
     [Binding]
    public class EventDefinition
    {

         [BeforeScenario]
         public void BeforeScenario()
         {
             /*var internetExplorerOptions = new InternetExplorerOptions
             {
                 IntroduceInstabilityByIgnoringProtectedModeSettings = true
             };*/
             var driver = new ChromeDriver();

             driver.Manage().Cookies.DeleteAllCookies();
			 driver.Manage().Window.Maximize();
             // var driver = new ChromeDriver();
             ScenarioContext.Current.Set(driver, "WebDriver");
             
         }


         [AfterScenario]
         public void AfterScenario()
         {
             var webDriver = ScenarioContext.Current.Get<IWebDriver>("WebDriver");
             if (ScenarioContext.Current.TestError != null)
             {
                 TakeScreenshot(webDriver);
             }
             webDriver.Close();
             webDriver.Quit();
         }

         [BeforeFeature]
         public static void BeforeFeature()
         {
             //var url = "https://stg-my.worldvision.ca/";
             var url = "http://localhost:49281/";
             FeatureContext.Current.Set(url, "WebSiteUrl");
         }

         private void TakeScreenshot(IWebDriver driver)
         {
             try
             {
                 string fileNameBase = string.Format("error_{0}",
                                                     DateTime.Now.ToString("yyyyMMdd_HHmmss"));

                 var artifactDirectory = Path.Combine(Directory.GetCurrentDirectory(), "testresults");
                 if (!Directory.Exists(artifactDirectory))
                     Directory.CreateDirectory(artifactDirectory);

                 string pageSource = driver.PageSource;
                 string sourceFilePath = Path.Combine(artifactDirectory, fileNameBase + "_source.html");
                 File.WriteAllText(sourceFilePath, pageSource, Encoding.UTF8);
                 Console.WriteLine("Page source: {0}", new Uri(sourceFilePath));

                 ITakesScreenshot takesScreenshot = driver as ITakesScreenshot;

                 if (takesScreenshot != null)
                 {
                     var screenshot = takesScreenshot.GetScreenshot();

                     string screenshotFilePath = Path.Combine(artifactDirectory, fileNameBase + "_screenshot.png");

                     screenshot.SaveAsFile(screenshotFilePath, ScreenshotImageFormat.Png);

                     Console.WriteLine("Screenshot: {0}", new Uri(screenshotFilePath));
                 }
             }
             catch (Exception ex)
             {
                 Console.WriteLine("Error while taking screenshot: {0}", ex);
             }
         }
    }
}
