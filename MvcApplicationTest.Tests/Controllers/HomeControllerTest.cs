﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplicationTest.Controllers;
using static MvcApplicationTest.Controllers.HomeController;
using System.Linq;
using System.Net.Http;

namespace MvcApplicationTest.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void GetProducts()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            IEnumerable<Product> dummy = HomeController._products;

            // Act
            IEnumerable<Product> result = cntrl.GetProducts();
                
            // Assert
            Assert.IsNotNull(result);
            Assert.AreSame(dummy, result);
            Assert.IsNotNull(result.Count());
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void GetProductsById()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            List<Product> dummy = HomeController._products;

            // Act
            Product result = cntrl.GetProduct(dummy[0].Id);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Name, dummy[0].Name);
            Assert.AreEqual(result.Id, dummy[0].Id);

        }

        [TestMethod]
        public void GetProductsByName()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            List<Product> dummy = HomeController._products;

            // Act
            IEnumerable<Product> result = cntrl.GetProductsByName(dummy[0].Name);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(dummy.Count > 0);
        }

        [TestMethod]
        public void PostProduct()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            List<Product> dummy = HomeController._products;

            Product product = dummy[0];

            // Act
            HttpResponseMessage result = cntrl.PostProduct(product);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DeleteProduct()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            List<Product> dummy = HomeController._products;

            // Act
            IEnumerable<Product> result = cntrl.DeleteProduct(dummy[0].Id);

            //Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void PutProduct()
        {
            //Arrange 
            HomeController cntrl = new HomeController();
            List<Product> dummy = HomeController._products;
            Product product = dummy[0];

            // Act
            HttpResponseMessage PutProductResp = cntrl.PutProduct(product);
            //Assert
            Assert.IsNotNull(PutProductResp);
        }
    }
}
