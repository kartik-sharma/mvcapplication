﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;

namespace SampleDemo1.StepDefinitions 
{
    [Binding]
    public class LoginSteps : StepDefinitionBase
    {
        [Given(@"I am on search page")]
        public void GivenIAmOnSearchPage()
        {
            Browser.Navigate().GoToUrl(Uri);
        }

        [When(@"I provide credentials ""(.*)"",""(.*)""")]
        public void WhenIProvideCredentials(string email, string password)
        {
            var loginPage = new LoginPage(Browser);
            loginPage.ProvideLoginCredentials(email, password);
        }
        
        [When(@"click on login button")]
        public void ThenClickOnLoginButton()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.DoLogin();
        }

        [Then(@"I should be logged in successfully")]
        public void ThenIShouldBeLoggedInSuccessfully()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.completeLogin();
        }

        [Then(@"I should see My Dashboard page")]
        public void ThenIShouldSeeMyDashboardPage()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.myDashboardPage();
        }

        [Then(@"I should see banner with alert message ""(.*)""")]
        public void ThenIShouldSeeBannerWithAlertMessage(string msg)
        {
            var loginPage = new LoginPage(Browser);
            loginPage.verifyAlertMsg(msg);
        }

        [Then(@"I should see message ""(.*)""")]
        public void ThenIShouldSeeMessage(string msg)
        {
            var loginPage = new LoginPage(Browser);
            loginPage.verifyLogout(msg);
        }


        [When(@"I enter the personal details ""(.*)"",""(.*)"",""(.*)"",""(.*)"",""(.*)""")]
        public void WhenIEnterThePersonalDetails(string existing, string type, string cell, string home, string street)
        {
            var loginPage = new LoginPage(Browser);
            loginPage.personalDetails(existing,type,cell,home,street);
        }

        [When(@"I click on Save")]
        public void WhenIClickOnSave()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.saveDetails();
        }

        [When(@"I click contact information")]
        public void WhenIClickContactInformation()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.contactInfo();
        }

        [Then(@"I can see my Donor ID")]
        public void ThenICanSeeMyDonorID()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.getDonorID();
        }

        [When(@"I click Logout")]
        public void WhenIClickLogout()
        {
            var loginPage = new LoginPage(Browser);
            loginPage.logOut();
        }


    }
}
