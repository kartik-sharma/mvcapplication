﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;

namespace SampleDemo1.StepDefinitions
{
    [Binding]
    public class VerifyEmailSteps : StepDefinitionBase
    {
        [Then(@"I receive a mail with email confirmation link in ""(.*)"",""(.*)""")]
        public void GivenIReceiveAMailWithEmailConfirmationLinkIn(string emailDomain, string email)
        {
            Browser.Navigate().GoToUrl(emailDomain);
            
            var registerFeature = new Register(Browser);
            registerFeature.verifyEmailAddress(email);
        }

        [When(@"I navigate to confirmation link")]
        public void GivenINavigateToConfirmationLink()
        {
            var registerFeature = new Register(Browser);
            registerFeature.navigateToConfLink();
        }


    }
}
