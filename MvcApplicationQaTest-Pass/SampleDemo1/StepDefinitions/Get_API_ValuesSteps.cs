﻿using System;
using TechTalk.SpecFlow;
using SampleDemo1.PageObjects;
using OpenQA.Selenium;

namespace SampleDemo1.StepDefinitions
{
    [Binding]
    public class Get_API_ValuesSteps : StepDefinitionBase
    {
        [Given(@"I launch the Test Application")]
        public void GivenILaunchTheTestApplication()
        {
			Browser.Navigate().GoToUrl(Uri);
			var homePage = new HomePage(Browser);
			homePage.waitForHomePageLoad();
		}
        
        [Given(@"I click on API menu")]
        public void GivenIClickOnAPIMenu()
        {
			System.Threading.Thread.Sleep(5000);
			var homePage = new HomePage(Browser);
			homePage.NavigateToPage("API");
        }
        
        [When(@"I click on get API values")]
        public void WhenIClickOnGetAPIValues()
        {
			System.Threading.Thread.Sleep(5000);
			var apiPage = new ApiPage(Browser);
			apiPage.selectLink("Get_Values");
		}

		[When(@"I click on Post API values")]
		public void WhenIClickOnPostAPIValues()
		{
			System.Threading.Thread.Sleep(5000);
			var apiPage = new ApiPage(Browser);
			apiPage.selectLink("Pos_Values");
		}

		[Then(@"the API Help Page is displayed")]
		public void ThenTheAPIHelpPageIsDisplayed()
		{
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("API Help Page")) {

			}
			else {
				throw new Exception("API Help page is not displayed");
			}
		}

		[Then(@"the response information page is displayed")]
		public void ThenTheResponseInformationPageIsDisplayed()
		{
			System.Threading.Thread.Sleep(5000);
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("api"))
			{

			}
			else
			{
				throw new Exception("API page is not displayed");
			}

		}


		[Then(@"the resposne information page is displayed")]
        public void ThenTheResposneInformationPageIsDisplayed()
        {
			System.Threading.Thread.Sleep(5000);
			String pageTitle = Browser.Title;
			if (pageTitle.Contains("api"))
			{

			}
			else
			{
				throw new Exception("API page is not displayed");
			}
			
        }
        
        [Then(@"I save the response information in Json format")]
        public void ThenISaveTheResponseInformationInJsonFormat()
        {
			var apiPage = new ApiPage(Browser);
			String jSonInfo=apiPage.getJSONResponse();
			if (jSonInfo.Length>0) {
			}
			else {
				throw new Exception("Json format");
			}


		}
    }
}
