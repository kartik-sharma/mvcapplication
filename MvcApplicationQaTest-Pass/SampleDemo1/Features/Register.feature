﻿Feature: Register
	Provide required details
	Click on register

@mytag
Scenario Outline: Register
	Given I am on register page
	And Click on register button
	And I provided required details "<FirstName>","<LastName>","<Email>","<PostalCode>","<Password>","<ConfirmPassword>"
	When I press register
	Then I should see banner with message "An email has been sent to <Email>"
	And I receive a mail with email confirmation link in "<EmailDomain>","<Email>"
	When I navigate to confirmation link
	Then I should see banner with success message "Your email has been verified successfully"
	When I provide credentials "<Email>","<Password>"
	And click on login button
	Then I should be logged in successfully
	And I should see banner with alert message "Thank you for registering!"
	When I enter the personal details "<Existing>","<Type>","<Cell>","<Home>","<Street>"
	And I click on Save
	Then I should see My Dashboard page


Examples: Data for user registration
| FirstName | LastName | Email                    | PostalCode | Password | ConfirmPassword | EmailDomain                 | Existing | Type       | Cell       | Home       | Street |
| Satishps  | Kumar    | satishps10@mailinator.com | L5T2Y4     | 123456   | 123456          | https://www.mailinator.com/ | No       | Individual | 4695312486 | 4695312487 | 1      |


