﻿using System;
using OpenQA.Selenium;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Windows.Forms;

namespace SampleDemo1.PageObjects
{
    public class HomePage : Page
    {

        private static class Locator
        {
			public static readonly By Home_Menu = By.LinkText("Home");
			public static readonly By API_Menu = By.LinkText("API");
			
        }

        public HomePage(IWebDriver browser)
            : base(browser)
        {

        }

        public void NavigateToPage(string pageName)
        {
			if (pageName.Contains("Home"))
			{
				Do(x => x.Click(), Locator.Home_Menu, true);
			}
			else
			{
				Do(x => x.Click(), Locator.API_Menu, true);
			}
		}
        public bool waitForHomePageLoad()
        {

			if (Page.waitTillElementisDisplayed(browser, Locator.Home_Menu, 5))
			{
				return true;
			}
			else
			{

				throw new Exception("The Home_Menu is not displayed");
			}
        }
        

    }
    
}
