﻿using System;
using OpenQA.Selenium;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Windows.Forms;

namespace SampleDemo1.PageObjects
{
    public class LoginPage : Page
    {

        private static class Locator
        {
            public static readonly By Email = By.Name("emailid");
            public static readonly By Password = By.Name("password");
            public static readonly By LoginButton = By.Id("loginradius-raas-submit-Login");

            public static readonly By FirstName = By.Id("firstname");
            public static readonly By Btn_No = By.XPath("//h3[./span[@class='ng-scope' and contains(text(),'Are you')]]/following-sibling::div[@class='form-group']//span[contains(text(),'Yes')]");
            
            public static readonly By CellPhone = By.Id("cellPhone");
            public static readonly By HomePhone = By.Id("homePhone");

            public static readonly By Address_Unit = By.Id("unit");
            public static readonly By Address_StreetNumber = By.Id("streetNumber");
            public static readonly By Address_StreetName = By.Id("streetName");
            public static readonly By Address_StreetType = By.Id("streetType");
            public static readonly By Address_StreetDirection = By.Id("streetDirection");
            public static readonly By Address_City = By.Id("city");
            public static readonly By Address_PostalCode = By.Id("postalCode");
            public static readonly By Address_DeliveryInfo = By.Id("deliveryInformation");
            public static readonly By Message = By.XPath("//span[contains(text(),'Thank you for registering! In order to display')]");
            public static readonly By Save = By.XPath("//span[text()='Save']");
            public static readonly By ContactInfo = By.Id("sidenavContactInformation");
            public static readonly By LogOut = By.XPath("//*[contains(text(),'Log Out')]");
            public static readonly By LogOutMessage = By.XPath("//span[contains(text(),'You have been logged out')]");
            public static readonly By DonorID = By.XPath("//div[@id='maincontent']/div/div/article/div/section/div/form/div[3]/div[2]");
            public static readonly By MyDashboardPage = By.XPath("//h1[contains(text(),'My Dashboard')]");
        }

        public LoginPage(IWebDriver browser)
            : base(browser)
        {

        }

        public bool ProvideLoginCredentials(string email, string password)
        {
            Do(x => x.SendKeys(email), Locator.Email, true);
            Do(x => x.SendKeys(password), Locator.Password, true);
            return true;
        }
        public bool personalDetails(string existing, string type,string cell,string home,string street)
        {
            Do(x => x.Click(), By.XPath("//div[@id='maincontent']/div/article/section[2]/form/div[3]/div/div/div/label/span[contains(text(),'" + existing + "')]"), true);
            Do(x => x.Click(), By.XPath("//div[@id='maincontent']/div/article/section[2]/form/div[3]/div[2]/div/div/label/span[contains(text(),'" + type + "')]"), true);
            Do(x => x.SendKeys(cell), Locator.CellPhone, true);
            Do(x => x.SendKeys(home), Locator.HomePhone, true);
            Do(x => x.SendKeys(street), Locator.Address_StreetNumber, true);
            return true;
        }
        public bool saveDetails()
        {
            Do(x => x.Click(), Locator.Save, true);
            return true;
        }
        public bool logOut()
        {
            Do(x => x.Click(), Locator.LogOut, true);
            return true;
        }
        public bool getDonorID()
        {
            if(waitTillElementisDisplayed(browser,Locator.DonorID,10))
            {
                Console.WriteLine(browser.FindElement(Locator.DonorID).Text);
                return true;
            }
            throw new Exception("Donor ID could not be found on page");
        }
        public bool contactInfo()
        {
            Do(x => x.Click(), Locator.ContactInfo, true);
            return true;
        }
        public bool myDashboardPage()
        {
            
            if (Page.waitTillElementisDisplayed(browser, Locator.MyDashboardPage, 5))
            { return true; }
            throw new Exception("The My Dashboard page is not displayed after saving Personal details");
        }
        public bool verifyAlertMsg(string msg)
        {
            // System.Threading.Thread.Sleep(5000);

            if (Page.waitTillElementisDisplayed(browser, Locator.Message, 15))
            {
                string actualmsg = browser.FindElement(Locator.Message).Text;

                if (actualmsg.Contains(msg))
                {
                    return true;
                }
                else
                {
                    throw new Exception("The expected message is not displayed. Expected:" + msg + ", Actual:" + actualmsg);
                }
            }
            else
            {
                throw new Exception("The register success alert message is not displayed.");
            }
        }
        public bool verifyLogout(string msg)
        {
            // System.Threading.Thread.Sleep(5000);

            if (Page.waitTillElementisDisplayed(browser, Locator.LogOutMessage, 15))
            {
                string actualmsg = browser.FindElement(Locator.LogOutMessage).Text;

                if (actualmsg.Contains(msg))
                {
                    return true;
                }
                else
                {
                    throw new Exception("The expected message is not displayed. Expected:" + msg + ", Actual:" + actualmsg);
                }
            }
            else
            {
                throw new Exception("The log out success message is not displayed");
            }
        }
        public void DoLogin()
        {
            Do(x => x.Click(), Locator.LoginButton, true);
            System.Threading.Thread.Sleep(3000);
           
        }

        public bool completeLogin()
        {
            

            if (Page.waitTillElementisDisplayed(browser, Locator.LogOut, 15))
            {  
                
               
                return true;
            }
            else
            {
                throw new Exception("Error while login.");
            }
        }


       

    }
    
}
