﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace SampleDemo1.PageObjects
{
	public class ApiPage : Page
	{
		private bool isLinkSelected;

		private static class Locator
		{
			public static readonly string Get_Values= "GET api/Values";
			public static readonly By Get_Values_Id = By.LinkText("API");

		}

		public bool selectLink(string linkText)
		{
			isLinkSelected = false;
			try
			{
				string getPostApi;
				if (linkText.Equals("Get_Values"))
				{
					getPostApi = "GET api/Values";
				}
				else{
					getPostApi = "POS api/Values";
				}
				browser.FindElement(By.LinkText(getPostApi)).Click();
				isLinkSelected = true;
				return isLinkSelected;
			}
			catch (Exception ex)
			{
				throw new Exception("Failed to click on API text");
				return isLinkSelected;
			}
		}

		public string getJSONResponse()
		{
			string jSonValue=browser.FindElement(By.XPath("(//h4[contains(text(),'application/json')]/..//pre)[1]")).Text;
			return jSonValue;
		}

		public ApiPage(IWebDriver browser)
            : base(browser)
        {

		}
	}
}
